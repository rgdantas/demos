extern crate cpal;
extern crate demos;
extern crate ggez;
extern crate rodio;

use ggez::{ conf, Context, event, GameResult, graphics, timer };
use rodio::dynamic_mixer::DynamicMixerController;
use std::{ env, path, time::Duration, sync::Arc };

use demos::*;

struct MainState
{
    synth: Synth,

    text: graphics::Text,
    frames: usize,
}

impl MainState
{
    fn new(context: &mut Context) -> GameResult<MainState>
    {
        let font = graphics::Font::new(context, "/DejaVuSans.ttf", 48)?;
        let text = graphics::Text::new(context, "Hello, World!", &font)?;

        let mut synth = Synth::new();
        synth.set_sample_rate(4000);
        synth.set_volume(0.4);

        synth.add_voice(Voice::new(4));

        synth.get_voice(0)
            .set_beat(0, PulseWave::new(
                Duration::from_millis(100),
                1000.,
                0.125,
            ));

        synth.get_voice(0)
            .set_beat(1, PulseWave::new(
                Duration::from_millis(100),
                1000.,
                0.25,
            ));

        synth.get_voice(0)
            .set_beat(2, PulseWave::new(
                Duration::from_millis(100),
                1000.,
                0.5,
            ));

        synth.get_voice(0)
            .set_beat(3, PulseWave::new(
                Duration::from_millis(100),
                1000.,
                0.75,
            ));

        Ok(MainState {
            synth: synth,

            text: text,
            frames: 0,
        })
    }
}

impl event::EventHandler for MainState
{
    fn update(&mut self, context: &mut Context) -> GameResult<()>
    {
        /*
        match self.frames % 60 {
            0 | 16 | 33 => {
                self.mixer.add(Snare::new(Duration::from_millis(100)));
            },
            49 => {
                self.mixer.add(HiHat::new(Duration::from_millis(100)));
            },
            _ => {},
        }
        */
        self.synth.update(timer::get_delta(context));

        Ok(())
    }

    fn draw(&mut self, context: &mut Context) -> GameResult<()>
    {
        graphics::clear(context);

        let dest_point = graphics::Point2::new(10., 10.);
        graphics::draw(context, &self.text, dest_point, 0.)?;

        self.frames += 1;
        if self.frames % 100 == 0 {
            println!("FPS: {}", ggez::timer::get_fps(context));
        }

        graphics::present(context);
        Ok(())
    }
}

fn main() -> GameResult<()>
{
    let conf    = conf::Conf::new();
    let context = &mut Context::load_from_conf("config", "ggez", conf)?;

    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        context.filesystem.mount(&path, true);
    }

    let state = &mut MainState::new(context)?;
    event::run(context, state)?;

    Ok(())
}

