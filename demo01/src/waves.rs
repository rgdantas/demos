use std::time::Duration;
use std::f32::consts::PI;

use rand::random;
use rodio::Source;
               
macro_rules! wave(
    ($name:ident $block:block) => {
        wave!($name(this) $block);
    };
    ($name:ident ($self:ident $(, $id:ident : $ty:ty)*) $block:block) => {
        #[derive(Clone, Debug)]
        pub struct $name
        {
            elapsed_ns: u64,
            total_ns:   u64,
            $($id: $ty),*
        }

        impl $name
        {
            pub fn new(duration: Duration $(, $id: $ty)*) -> $name
            {                                
                let total_ns = duration.as_secs() * 1_000_000_000 +
                    duration.subsec_nanos() as u64;

                $name {
                    elapsed_ns: 0,
                    total_ns:   total_ns,
                    $($id: $id),*
                }
            }
         }

        impl Source for $name
        {
            #[inline]
            fn current_frame_len(&self) -> Option<usize>
            {
                let remaining_ns = (self.total_ns - self.elapsed_ns) as f32;
                let frame_ratio  = 48_000. / 1_000_000_000.; 
                Some((remaining_ns * frame_ratio) as usize)
            }

            #[inline]
            fn channels(&self) -> u16
            {
                1
            }

            #[inline]
            fn sample_rate(&self) -> u32
            {
                48000
            }

            #[inline]
            fn total_duration(&self) -> Option<Duration>
            {
                let secs  = self.total_ns / 1_000_000_000;
                let nanos = self.total_ns % 1_000_000_000;
                Some(Duration::new(secs as u64, nanos as u32))
            }
        }

        impl Iterator for $name
        {
            type Item = f32;

            #[inline]
            fn next(&mut self) -> Option<Self::Item>
            {
                if self.elapsed_ns > self.total_ns {
                    return None;
                }

                let $self  = self;
                let result = $block;

                $self.elapsed_ns += 1_000_000_000 / 48_000;
                result
            }
        }
    }
);

wave!(SineWave(this, frequency: f32) {
    let elapsed = this.elapsed_ns as f32 / 1_000_000_000.;
    let omega   = 2. * PI * this.frequency;
    let value   = (omega * elapsed).sin();
    Some(value)
});

wave!(PulseWave(this, frequency: f32, sustain: f32) {
    let elapsed = this.elapsed_ns as f32 / 1_000_000_000.;
    let period  = 1. / this.frequency;

    let delta_t = elapsed / period;
    let delta_t = delta_t - delta_t.floor();

    Some(if delta_t <= this.sustain { 1. } else { -1. })
});

wave!(WhiteNoise {
    Some(2. * random::<f32>() - 1.)
});

wave!(Snare(this) {
    let ratio = this.elapsed_ns as f32 / this.total_ns as f32;
    Some((2. * random::<f32>() - 1.) * (1. - ratio))
});

wave!(HiHat(this) {
    let elapsed_ns = this.elapsed_ns as f32;
    let quarter_ns = this.total_ns as f32 / 4.;

    let ratio = if elapsed_ns <= quarter_ns {
        elapsed_ns / quarter_ns
    } else {
        let x = (elapsed_ns - quarter_ns) / (3. * quarter_ns);
        let r = -(x + 0.01).log(10.) / 2.;

        if r > 0. { r } else { 0. }
    };

    Some((2. * random::<f32>() - 1.) * ratio)
});
