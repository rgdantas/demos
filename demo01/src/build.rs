use std::{ env, error::Error, fs, io };

fn main() -> Result<(), Box<Error>>
{   
    eprintln!("Defining directory paths");
    let man_dir = env::var("CARGO_MANIFEST_DIR")? + "/";
    let out_dir = env::var("OUT_DIR")? + "/../../../";

    let src_dll = man_dir.clone() + "dylib/SDL2.dll";
    let dst_dll = out_dir.clone() + "SDL2.dll";
                                    
    eprintln!("Copying SDL2.dll");
    fs::copy(src_dll, dst_dll)?;

    let res_src_dir = man_dir.clone() + "resources/";
    let res_dst_dir = out_dir.clone() + "resources/";

    eprintln!("Creating resources directory on target");
    create_dir_unless_exists(&res_dst_dir)?;

    eprintln!("Copying resources");
    let resources = vec![
        "DejaVuSans.ttf",
    ];
        
    for res in resources {
        fs::copy(res_src_dir.clone() + res, res_dst_dir.clone() + res)?;
    }

    Ok(())
}

fn create_dir_unless_exists(path: &str) -> io::Result<()>
{
    if let Err(e) = fs::create_dir_all(path) {
        if e.kind() != io::ErrorKind::AlreadyExists {
            eprintln!("{}", path);
            return Err(e);
        }
    }

    Ok(())
}
