use std::time::Duration;

use cpal::Sample as CpalSample;
use rodio::Sample;
use rodio::Source;

pub fn binary<I>(input: I) -> Binary<I>
    where I: Source,
          I::Item: Sample
{
    Binary {
        input: input,
    }
}

#[derive(Clone, Debug)]
pub struct Binary<I>
    where I: Source,
          I::Item: Sample
{
    input: I,
}

impl<I> Iterator for Binary<I>
    where I: Source,
          I::Item: Sample + CpalSample
{
    type Item = I::Item;

    #[inline]
    fn next(&mut self) -> Option<I::Item> {
        let next = match self.input.next() {
            Some(next) => next.to_f32(),
            None => { return None; }
        };

        let value = if next <= 0. { -1. } else { 1. };
        Some(CpalSample::from(&value))
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.input.size_hint()
    }
}

impl<I> ExactSizeIterator for Binary<I>
    where I: Source + ExactSizeIterator,
          I::Item: Sample + CpalSample
{
}

impl<I> Source for Binary<I>
    where I: Source,
          I::Item: Sample + CpalSample
{
    #[inline]
    fn current_frame_len(&self) -> Option<usize> {
        self.input.current_frame_len()
    }

    #[inline]
    fn channels(&self) -> u16 {
        self.input.channels()
    }

    #[inline]
    fn sample_rate(&self) -> u32 {
        self.input.sample_rate()
    }

    #[inline]
    fn total_duration(&self) -> Option<Duration> {
        self.input.total_duration()
    }
}
