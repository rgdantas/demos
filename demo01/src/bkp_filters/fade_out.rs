use std::time::Duration;

use cpal::Sample as CpalSample;
use rodio::Sample;
use rodio::Source;

pub fn fadeout<I>(input: I, duration: Duration) -> FadeOut<I>
    where I: Source,
          I::Item: Sample
{
	let duration = duration.as_secs() * 1_000_000_000 + duration.subsec_nanos() as u64;

    FadeOut {
        input: input,
        remaining_ns: duration as f32,
        total_ns: duration as f32,
    }
}

#[derive(Clone, Debug)]
pub struct FadeOut<I>
    where I: Source,
          I::Item: Sample
{
    input: I,
    remaining_ns: f32,
    total_ns: f32,
}

impl<I> Iterator for FadeOut<I>
    where I: Source,
          I::Item: Sample + CpalSample
{
    type Item = I::Item;

    #[inline]
    fn next(&mut self) -> Option<I::Item> {
        if self.remaining_ns <= 0.0 {
            return Some(CpalSample::from(&0.));
        }

        let factor = self.remaining_ns / self.total_ns;
        self.remaining_ns -= 1000000000.0 / 
            (self.input.sample_rate() as f32 * self.input.channels() as f32);
        self.input.next().map(|value| value.amplify(factor))
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.input.size_hint()
    }
}

impl<I> ExactSizeIterator for FadeOut<I>
    where I: Source + ExactSizeIterator,
          I::Item: Sample + CpalSample
{
}

impl<I> Source for FadeOut<I>
    where I: Source,
          I::Item: Sample + CpalSample
{
    #[inline]
    fn current_frame_len(&self) -> Option<usize> {
        self.input.current_frame_len()
    }

    #[inline]
    fn channels(&self) -> u16 {
        self.input.channels()
    }

    #[inline]
    fn sample_rate(&self) -> u32 {
        self.input.sample_rate()
    }

    #[inline]
    fn total_duration(&self) -> Option<Duration> {
        self.input.total_duration()
    }
}
