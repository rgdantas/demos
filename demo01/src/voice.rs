use std::collections::HashMap;
use std::time::Duration;
use std::sync::Arc;

use rodio::{ default_output_device, Sample, Sink, Source, source::Zero };

use waves::*;

pub struct Voice<S>
    where S: Source,
          S::Item: Sample
{
    sink: Sink,

    beats: HashMap<u32, Arc<S>>,
    beat_loop: u32,
}

impl<S> Voice<S>
    where S: Source + Send + Sync,
          S::Item: Sample + Send
{
    pub fn new(beat_loop: u32) -> Self
    {
        let device = default_output_device().unwrap();
        let sink   = Sink::new(&device);

        Voice {
            sink:  sink,
            beats: HashMap::new(),
            beat_loop: beat_loop,
        }
    }

    pub fn set_beat(&mut self, beat: u32, source: S)
    {
        self.beats.insert(beat, Arc::new(source));
    }

    pub fn update(&mut self, beat: u32)
    {
        let beat = if self.beat_loop > 0 {
            beat % self.beat_loop
        } else {
            beat
        };

        match self.beats.get_mut(&beat) {
            Some(source) => if let Some(s) = Arc::get_mut(source) {
                self.sink.append(s);
            },
            None => {},
        }

        let duration = Duration::from_millis(100);

        let beat = match beat % 4 {
            0 => 0.125,
            1 => 0.25,
            2 => 0.5,
            3 => 0.75,
            _ => 0.125,
        };

        println!("{}", beat);

        let pulse = PulseWave::new(duration, 1000., beat);
        self.sink.append(pulse);
    }
}
