use std::time::Duration;
use std::f32::consts::PI;

use rodio::Source;

#[derive(Clone, Debug)]
pub struct SineWave
{
    frequency: f32,
    num_sample: u32,
    max_samples: u32,
}

impl SineWave 
{
    pub fn new(frequency: f32, dur: Duration) -> Self
    {
        let fdur = dur.as_secs() as f32 + (dur.subsec_nanos() as f32 / 1_000_000_000.);

        SineWave {
            frequency: frequency,
            num_sample: 0,
            max_samples: (48000. * fdur).ceil() as u32,
        }
    }
}

impl Iterator for SineWave
{
    type Item = f32;

    #[inline]
    fn next(&mut self) -> Option<f32>
    {
        if self.num_sample >= self.max_samples {
            return None;
        }

        let dt    = self.num_sample as f32 / 48000.;
        let omega = 2. * PI * self.frequency;
        let value = (omega * dt).sin();

        self.num_sample = self.num_sample.wrapping_add(1);
        Some(value)
    }
}

impl Source for SineWave
{
    fn current_frame_len(&self) -> Option<usize>
    {
        Some((self.max_samples - self.num_sample) as usize)
    }

    fn channels(&self) -> u16
    {
        1
    }

    fn sample_rate(&self) -> u32
    {
        48000
    }

    fn total_duration(&self) -> Option<Duration>
    {
        let duration = self.max_samples as f32 / 48000.;
        let secs     = duration.abs();
        let nanos    = (duration - secs) * 1_000_000_000.;

        Some(Duration::new(secs as u64, nanos as u32))
    }
}
