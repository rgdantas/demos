mod sawtooth;
mod sine;
mod square;
mod triangle;
mod white_noise;

pub use self::sawtooth::*;
pub use self::sine::*;
pub use self::square::*;
pub use self::triangle::*;
pub use self::white_noise::*;

#[macro_export]
macro_rules! note(
    ($wave: ident, $dur: expr) => {{
        use std::time::Duration;
        $wave::new(Duration::from_millis($dur))
    }};
    ($wave: ident, $freq: expr, $dur: expr) => {{
        use std::time::Duration;
        $wave::new($freq as f32, Duration::from_millis($dur))
    }}
);
