use std::time::Duration;

use rodio::Source;

// Good for flutes.

#[derive(Clone, Debug)]
pub struct TriangleWave
{
    frequency: f32,
    num_sample: u32,
    max_samples: u32,
}

impl TriangleWave 
{
    pub fn new(frequency: f32, dur: Duration) -> Self
    {
        let fdur = dur.as_secs() as f32 + (dur.subsec_nanos() as f32 / 1_000_000_000.);

        TriangleWave {
            frequency: frequency,
            num_sample: 0,
            max_samples: (48000. * fdur).ceil() as u32,
        }
    }
}

impl Iterator for TriangleWave
{
    type Item = f32;

    fn next(&mut self) -> Option<f32>
    {
        if self.num_sample >= self.max_samples {
            return None;
        }

        let df    = (self.num_sample as f32 / 48000.) * self.frequency;
        let value = 2. * (2. * (df - (0.5 + df).floor()) - 1.).abs() - 1.;

        self.num_sample = self.num_sample.wrapping_add(1);
        Some(value)
    }
}

impl Source for TriangleWave
{
    fn current_frame_len(&self) -> Option<usize>
    {
        Some((self.max_samples - self.num_sample) as usize)
    }

    fn channels(&self) -> u16
    {
        1
    }

    fn sample_rate(&self) -> u32
    {
        48000
    }

    fn total_duration(&self) -> Option<Duration>
    {
        let duration = self.max_samples as f32 / 48000.;
        let secs     = duration.abs();
        let nanos    = (duration - secs) * 1_000_000_000.;

        Some(Duration::new(secs as u64, nanos as u32))
    }
}
