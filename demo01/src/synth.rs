use std::iter::Iterator;
use std::time::Duration;

use rodio::{ Sample, Source };

use voice::Voice;

pub struct Synth<S>
    where S: Source,
          S::Item: Sample
{
    sample_rate: u32,
    bpm:         u32,
    voices:      Vec<Voice<S>>,
    volume:      f32,

    beat_duration: Duration,
    beat_elapsed:  Duration,
    beat_current:  u32,
}

impl<S> Synth<S>
    where S: Source + Send + Sync,
          S::Item: Sample + Send
{

    pub fn new() -> Self
    {
        Synth {
            sample_rate: 48000,
            bpm:         150,
            volume:      0.5,
            voices:      vec![],
            
            beat_duration: Duration::from_secs(60) / 150,
            beat_elapsed:  Duration::from_secs(0),
            beat_current:  0,
        }
    }

    pub fn set_sample_rate(&mut self, sample_rate: u32)
    {
        self.sample_rate = sample_rate;
    }

    pub fn set_bpm(&mut self, bpm: u32)
    {
        self.bpm           = bpm;
        self.beat_duration = Duration::from_secs(60) / bpm;
    }

    pub fn set_volume(&mut self, volume: f32)
    {
        self.volume = volume;
    }

    pub fn add_voice(&mut self, voice: Voice<S>)
    {
        self.voices.push(voice)
    }

    pub fn get_voice(&self, index: usize) -> &Voice<S>
    {
        &self.voices[index]
    }

    pub fn update(&mut self, delta: Duration)
    {
        self.beat_elapsed += delta;

        if self.beat_elapsed > self.beat_duration {
            self.beat_elapsed  = Duration::from_secs(0);
            self.beat_current += 1;

            for voice in self.voices.iter_mut() {
                voice.update(self.beat_current)
            }
        }
    }
}
