use rodio::{ source::*, Source };
use std::time::Duration;

use filters::*;
use waves::*;

pub fn snare(duration: Duration) -> FadeOut<Binary<WhiteNoise>>
{
    fadeout(binary(WhiteNoise::new(duration)), duration)
}

pub fn hithat_open(duration: Duration)
    -> Mix<FadeIn<Binary<WhiteNoise>>, Delay<FadeOut<Binary<WhiteNoise>>>>
{
    let in_dur  = duration / 8;
    let out_dur = duration - in_dur;

    let noise = |dur| binary(WhiteNoise::new(dur));

    let fade_in  = noise(in_dur).fade_in(in_dur);
    let fade_out = fadeout(noise(out_dur), out_dur);

    fade_in.mix(fade_out.delay(out_dur))
}

pub fn hithat_closed(duration: Duration) -> FadeIn<Binary<WhiteNoise>>
{
    binary(WhiteNoise::new(duration)).fade_in(duration / 2)
}
